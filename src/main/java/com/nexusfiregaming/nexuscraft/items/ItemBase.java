package com.nexusfiregaming.nexuscraft.items;

import com.nexusfiregaming.nexuscraft.Main;
import com.nexusfiregaming.nexuscraft.init.ModItems;
import com.nexusfiregaming.nexuscraft.tabs.NexusCraftTab;
import com.nexusfiregaming.nexuscraft.util.IHasModel;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBase extends Item implements IHasModel
{

	public ItemBase(String name)
	{
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(CreativeTabs.SEARCH);
		setCreativeTab(NexusCraftTab.tabNexusCraft);
		
		ModItems.ITEMS.add(this);
	}
	
	@Override
	public void registerModels()
	{
		Main.proxy.registerItemRenderer(this, 0, "inventory");
	}

}
