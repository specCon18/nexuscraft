package com.nexusfiregaming.nexuscraft.items;

import com.nexusfiregaming.nexuscraft.init.ModItems;
import com.nexusfiregaming.nexuscraft.tabs.NexusCraftTab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.Item.ToolMaterial;

public class HoeBase extends ItemHoe
{
	public HoeBase(String name, ToolMaterial material)
	{
		super(material);
		this.setRegistryName(name);
		this.setUnlocalizedName(name);
		setCreativeTab(CreativeTabs.SEARCH);
		setCreativeTab(NexusCraftTab.tabNexusCraft);
		
		ModItems.ITEMS.add(this);
	}
}
