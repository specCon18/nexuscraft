package com.nexusfiregaming.nexuscraft.items;

import com.nexusfiregaming.nexuscraft.init.ModItems;
import com.nexusfiregaming.nexuscraft.tabs.NexusCraftTab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.Item.ToolMaterial;

public class AxeBase extends ItemAxe
{
	protected AxeBase(String name, ToolMaterial material, float damage, float speed)
	{
		super(material);
		this.setRegistryName(name);
		this.setUnlocalizedName(name);
		setCreativeTab(CreativeTabs.SEARCH);
		setCreativeTab(NexusCraftTab.tabNexusCraft);
		
		ModItems.ITEMS.add(this);
	}
}
