package com.nexusfiregaming.nexuscraft.util;

public interface IHasModel
{
	public void registerModels();
}
