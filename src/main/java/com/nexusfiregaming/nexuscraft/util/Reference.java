package com.nexusfiregaming.nexuscraft.util;

public class Reference
{
	//Version
    private static final String MC_VERSION = "1.12.2";
    private static final String MOD_VERSION = "1.0.0";
    private static final String MOD_VERSION_REVISION = "";
	
    //@Mod Strings
	public static final String MODID = "nexuscraft";
    public static final String NAME = "Nexus Craft RPG";
    public static final String VERSION = MC_VERSION + "-" + MOD_VERSION + MOD_VERSION_REVISION;
    
    //Accepted MC Versions
    public static final String ACCEPTED_VERSIONS = "[1.12.2]";
    
    //Proxy Classes
    public static final String CLIENT_PROXY_CLASS = "com.nexusfiregaming.nexuscraft.proxy.ClientProxy";
    public static final String COMMON_PROXY_CLASS = "com.nexusfiregaming.nexuscraft.proxy.CommonProxy";
	
    //GUI Classes
    public static final int GUI_Alloy_Furnace = 0;
}
