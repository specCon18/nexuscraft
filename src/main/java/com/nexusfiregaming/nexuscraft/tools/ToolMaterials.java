package com.nexusfiregaming.nexuscraft.tools;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class ToolMaterials
{
	public static ToolMaterial steel = EnumHelper.addToolMaterial("steel", 2, 400, 6.5F, 2.5F, 18);
}
