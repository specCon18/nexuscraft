package com.nexusfiregaming.nexuscraft.tools.init;

import java.util.ArrayList;
import java.util.List;

import com.nexusfiregaming.nexuscraft.items.HoeBase;
import com.nexusfiregaming.nexuscraft.items.PickaxeBase;
import com.nexusfiregaming.nexuscraft.items.ShovelBase;
import com.nexusfiregaming.nexuscraft.items.SwordBase;
import com.nexusfiregaming.nexuscraft.tools.ToolMaterials;

import net.minecraft.item.Item;

public class ModTools
{
	public static final List<Item> TOOLS = new ArrayList<Item>();
	
	public static final Item Steel_Pickaxe = new PickaxeBase("steel_pickaxe", ToolMaterials.steel);
	public static final Item Steel_Shovel = new ShovelBase("steel_shovel", ToolMaterials.steel);
	public static final Item Steel_Sword = new SwordBase("steel_sword", ToolMaterials.steel);
	public static final Item Steel_Hoe = new HoeBase("steel_hoe", ToolMaterials.steel);
}
