package com.nexusfiregaming.nexuscraft.tabs;

import com.nexusfiregaming.nexuscraft.init.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class NexusCraftTab
{
	public static CreativeTabs tabNexusCraft = new CreativeTabs("tab_nc")
	{
		@Override
		public ItemStack getTabIconItem()
		{
			return new ItemStack(ModItems.Gold_Piece);
		}
		
		@Override
		public boolean hasSearchBar()
		{
			return true;
		}
		
	}.setBackgroundImageName("item_search.png");
}
