package com.nexusfiregaming.nexuscraft.blocks;

import com.nexusfiregaming.nexuscraft.Main;
import com.nexusfiregaming.nexuscraft.init.ModBlocks;
import com.nexusfiregaming.nexuscraft.init.ModItems;
import com.nexusfiregaming.nexuscraft.tabs.NexusCraftTab;
import com.nexusfiregaming.nexuscraft.util.IHasModel;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block implements IHasModel
{
	
	public BlockBase(String name, Material material)
	{
		super(material);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(CreativeTabs.SEARCH);
		setCreativeTab(NexusCraftTab.tabNexusCraft);
		
		ModBlocks.BLOCKS.add(this);
		ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
	}

	@Override
	public void registerModels()
	{
		Main.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, "inventory");
	}
	
}
