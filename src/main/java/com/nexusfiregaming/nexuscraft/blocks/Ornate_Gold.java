package com.nexusfiregaming.nexuscraft.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class Ornate_Gold extends BlockBase
{
	public Ornate_Gold(String name, Material material)
	{
		super(name, material);
		this.setSoundType(SoundType.METAL);
		this.setHardness(5.0F);
		this.setResistance(30.0F);
		this.setHarvestLevel("pickaxe", 1);
		this.setLightLevel(12.0F);
	}
}
