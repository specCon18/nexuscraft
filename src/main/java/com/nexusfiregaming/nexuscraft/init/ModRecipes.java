package com.nexusfiregaming.nexuscraft.init;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes
{
	public static void init()
	{
		GameRegistry.addSmelting(Items.IRON_INGOT, new ItemStack(ModItems.Steel_Ingot), 5.0F);
		GameRegistry.addSmelting(ModItems.Fused_Tin_Copper, new ItemStack(ModItems.Bronze_Ingot), 5.0F);
		
		//Currency to nuggets
		GameRegistry.addSmelting(ModItems.Platinum_Piece, new ItemStack(ModItems.Platinum_Nugget), 0.0F);
		GameRegistry.addSmelting(ModItems.Copper_Piece, new ItemStack(ModItems.Copper_Nugget), 0.0F);
		GameRegistry.addSmelting(ModItems.Silver_Piece, new ItemStack(ModItems.Silver_Nugget), 0.0F);
		GameRegistry.addSmelting(ModItems.Gold_Piece, new ItemStack(Items.GOLD_NUGGET), 0.0F);
		GameRegistry.addSmelting(ModItems.Vape_Mod, new ItemStack(ModItems.Steel_Nugget), 0.0F);
		GameRegistry.addSmelting(ModItems.Vape_Mod, new ItemStack(Items.GOLD_INGOT), 0.0F);
	}
	
}
