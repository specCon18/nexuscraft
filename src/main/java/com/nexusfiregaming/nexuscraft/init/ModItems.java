package com.nexusfiregaming.nexuscraft.init;


import java.util.ArrayList;
import java.util.List;

import com.nexusfiregaming.nexuscraft.items.HoeBase;
import com.nexusfiregaming.nexuscraft.items.ItemBase;
import com.nexusfiregaming.nexuscraft.items.PickaxeBase;
import com.nexusfiregaming.nexuscraft.items.ShovelBase;
import com.nexusfiregaming.nexuscraft.items.SwordBase;
import com.nexusfiregaming.nexuscraft.tools.ToolMaterials;

import net.minecraft.item.Item;

public class ModItems
{

		public static final List<Item> ITEMS = new ArrayList<Item>();
		
		public static final Item Gold_Piece = new ItemBase("goldpiece");
		public static final Item Silver_Piece = new ItemBase("silverpiece");
		public static final Item Copper_Piece = new ItemBase("copperpiece");
		public static final Item Platinum_Piece = new ItemBase("platinumpiece");
		public static final Item Coin_Purse = new ItemBase("coin_purse");
		public static final Item Steel_Ingot = new ItemBase("steel_ingot");
		public static final Item Steel_Nugget = new ItemBase("steel_nugget");
		public static final Item Silver_Ingot = new ItemBase("silver_ingot");
		public static final Item Silver_Nugget = new ItemBase("silver_nugget");
		public static final Item Platinum_Ingot = new ItemBase("platinum_ingot");
		public static final Item Platinum_Nugget = new ItemBase("platinum_nugget");
		public static final Item Copper_Ingot = new ItemBase("copper_ingot");
		public static final Item Copper_Nugget = new ItemBase("copper_nugget");
		public static final Item Tin_Ingot = new ItemBase("tin_ingot");
		public static final Item Tin_Nugget = new ItemBase("tin_nugget");
		public static final Item Vape_Mod = new ItemBase("vape_mod");
		public static final Item Fusing_Hammer = new ItemBase("fusing_hammer");
		public static final Item Fused_Tin_Copper = new ItemBase("fused_tin_copper");
		public static final Item Bronze_Ingot = new ItemBase("bronze_ingot");
	
}
