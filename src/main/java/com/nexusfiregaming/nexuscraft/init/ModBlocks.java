package com.nexusfiregaming.nexuscraft.init;

import java.util.ArrayList;
import java.util.List;

import com.nexusfiregaming.nexuscraft.blocks.Copper_Block;
import com.nexusfiregaming.nexuscraft.blocks.GuildStation;
import com.nexusfiregaming.nexuscraft.blocks.Ornate_Gold;
import com.nexusfiregaming.nexuscraft.blocks.Platinum_Block;
import com.nexusfiregaming.nexuscraft.blocks.Silver_Block;
import com.nexusfiregaming.nexuscraft.blocks.Steel_Block;
import com.nexusfiregaming.nexuscraft.blocks.Swirl_Block;
import com.nexusfiregaming.nexuscraft.blocks.Tin_Block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlocks
{

	public static final List<Block> BLOCKS = new ArrayList<Block>();
	
	public static final Block GUILD_STATION = new GuildStation("guild_station", Material.IRON);
	public static final Block Steel_Block = new Steel_Block("steel_block", Material.IRON);
	public static final Block Silver_Block = new Silver_Block("silver_block", Material.IRON);
	public static final Block Platinum_Block = new Platinum_Block("platinum_block", Material.IRON);
	public static final Block Copper_Block = new Copper_Block("copper_block", Material.IRON);
	public static final Block Tin_Block = new Tin_Block("tin_block", Material.IRON);
	public static final Block Swirl_Block = new Swirl_Block("swirl_block", Material.IRON);
	public static final Block Ornate_Gold = new Ornate_Gold("ornate_gold", Material.IRON);
	
}
